var gulp = require("gulp");

var map = require('@flairlabs/gulp-utils')
map.forEach((task) => {
  gulp.task(task.functionName, task.deps, task.fn)
});