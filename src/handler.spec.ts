import { describe } from "mocha";
import { expect } from "chai";

describe("Sample Unit test", () => {
    it("Test should return 200", () => {
        let testStatusCode = process.env.TEST_STATUSCODE;
        expect(testStatusCode).to.equals('200');
    });
});
