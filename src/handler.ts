import { Handler, Context, Callback } from "aws-lambda";
import * as _ from "lodash";

interface preProcResponse {
  statusCode: number;
  body: any;
}

const preprocessing: Handler = (
  event: any,
  context: Context,
  callback: Callback
) => {
  let request = _.get(event, "body");
  let _body = _.get(request, "body");
  _body = JSON.parse(_body);
  let _type = _body.request.type;
  console.log("BODY:" + JSON.stringify(_body));
  let _sessionId = _.get(_body, "session.sessionId");
  let _userId = _.get(_body, "session.user.userId");
  let _locale = _.get(_body, "request.locale");
  let _apiEndpoint = _.get(_body, "context.System.apiEndpoint");
  let _apiAccessToken = _.get(_body, "context.System.apiAccessToken");
  let _requestId = _.get(_body, "request.requestId");
  let display = _.get(_body, "context.System.device.supportedInterfaces.Display");
  let _isDisplayEnabled = display != undefined && display != {};
  let _appName = _.get(request, "pathParameters.app");
  let _stage = _.get(request, "pathParameters.stage");
  let _isNewSession: boolean = _.get(_body, "session.new");
  let _extras = {
    apiEndpoint: _apiEndpoint,
    apiAccessToken: _apiAccessToken,
    requestId: _requestId
  };
  let _arguments: any = [];
  let intentMap: any = {
    "AMAZON.MoreIntent": "MORE",
    "AMAZON.StopIntent": "END",
    "AMAZON.CancelIntent": "END",
    "AMAZON.HelpIntent": "HELP",
    "AMAZON.RepeatIntent": "REPEAT",
    "AMAZON.NextIntent": "NEXT",
    "AMAZON.FallbackIntent": "UNKNOWN",
    "AMAZON.YesIntent": "YES",
    "AMAZON.NoIntent": "NO",
    "AMAZON.ResumeIntent": "RESUME",
    "AMAZON.StartOverIntent": "RESTART"
  };
  let response: preProcResponse = {
    statusCode: 200,
    body: {}
  };
  switch (_type) {
    case "IntentRequest": {
      let _verb = _body.request.intent.name;
      if (intentMap[_verb]) {
        _verb = intentMap[_verb];
      }

      let slots = _body.request.intent.slots;
      for (var key in slots) {
        if (slots.hasOwnProperty(key)) {
          let _arg = {};
          let slot = _.get(slots, key);
          _.set(_arg, "name", slot.name);
          _.set(_arg, "value", slot.value);
          let resolutions = _.get(slot, "resolutions");
          if (resolutions) {
            let _id = _.get(
              resolutions,
              "resolutionsPerAuthority[0].values[0].value.id"
            );
            _.set(_arg, "id", _id);
          }
          _arguments.push(_arg);
        }
      }
      response.body = {
        verb: _verb,
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    case "LaunchRequest": {
      response.body = {
        verb: "LAUNCH",
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    case "SessionEndedRequest": {
      response.body = {
        verb: "SESSIONEND",
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    case "Connections.Response": {
      let _arg = {};
      _.set(_arg, "statusCode", _body.request.status.code);
      //_.set(_arg, "statusMessage", _body.request.status.message);
      _.set(_arg, "purchaseResult", _body.request.payload.purchaseResult);
      _.set(_arg, "productId", _body.request.payload.productId);
      //_.set(_arg, "payloadMessage", _body.request.payload.message);
      _.set(_arg, "token", _body.request.token);
      _.set(_arg, "name", _body.request.name);
      _arguments.push(_arg);
      response.body = {
        verb: "ON_PURCHASE",
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    case "CanFulfillIntentRequest": {
      let _verb = "CAN_FULFILL";
      if (intentMap[_verb]) {
        _verb = intentMap[_verb];
      }
      let _canFulfillIntentName = _body.request.intent.name;
      _.set(_extras, "canFulfillIntentName", _canFulfillIntentName);
      let slots = _body.request.intent.slots;
      for (var key in slots) {
        if (slots.hasOwnProperty(key)) {
          let _arg = {};
          let slot = _.get(slots, key);
          _.set(_arg, "name", slot.name);
          _.set(_arg, "value", slot.value);
          _arguments.push(_arg);
        }
      }
      response.body = {
        verb: _verb,
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    case "Display.ElementSelected": {
      let token = _.get(_body, "request.token");
      _arguments.push(token);
      response.body = {
        verb: "DISPLAY_ELEMENT_SELECTED",
        args: _arguments,
        sessionId: _sessionId,
        userId: _userId,
        locale: _locale,
        isDisplayEnabled: _isDisplayEnabled,
        appName: _appName,
        stage: _stage,
        isNewSession: _isNewSession,
        extras: _extras
      };
      break;
    }
    default: {
      response.statusCode = 501;
      response.body = {
        shouldEndSession: false
      };
      break;
    }
  }
  callback(undefined, response);
};

export { preprocessing };
